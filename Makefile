#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/03 18:20:28 by alangloi          #+#    #+#              #
#    Updated: 2020/10/22 19:45:49 by alangloi         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

.PHONY: all test clean fclean re

CC = gcc

FLAGS = -Wall -Wextra -Werror

NAME = libftprintf.a

HEADER = -Iincludes

SRC_PATH = ./sources
LIB_PATH = ./libft
INC_PATH = ./includes
OBJ_PATH = ./objects

SOURCES = ft_printf.c \
	check_convert.c \
	check_flags.c \
	flags_init.c \
	parsing.c \
	print_c.c \
	print_i.c \
	print_p.c \
	print_per.c \
	print_s.c \
	print_u.c \
	print_x.c

SRC = $(addprefix $(SRC_PATH)/,$(SOURCES))
OBJ = $(addprefix $(OBJ_PATH)/,$(SOURCES:.c=.o))
LIB = ./libft/libft.a

$(NAME): $(OBJ)
	cd $(LIB_PATH) && $(MAKE)
	cp $(LIB) $(NAME)
	ar rcs $@ $^

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	mkdir $(OBJ_PATH) 2> /dev/null || true
	$(CC) $(FLAGS) $(HEADER) -c -o $@ $^

all: $(NAME)

clean:
	cd $(LIB_PATH) && $(MAKE) clean
	rm -rf $(OBJ_PATH) main.o

fclean: clean
	cd $(LIB_PATH) && $(MAKE) fclean
	rm $(NAME)

re: fclean all
