/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_x.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/06 16:39:56 by alangloi          #+#    #+#             */
/*   Updated: 2020/11/05 15:42:39 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static void	print_minus_x(t_printf *l, int up, unsigned int *u)
{
	if ((l->f & M) && (!(l->f & P) || ((l->p == 0) && (l->w > 0)
					&& ((unsigned int)u == 0))))
	{
		if (!((l->f & P) && (l->p == 0) && ((unsigned int)u == 0)))
		{
			if (up == 1)
				ft_putstr_fd(ft_utoa_base((unsigned int)u, 16, 1), 1);
			else
				ft_putstr_fd(ft_utoa_base((unsigned int)u, 16, 0), 1);
		}
		else
		{
			l->ret++;
			ft_putchar_fd(' ', 1);
		}
		l->w -= l->len;
		l->len = 0;
	}
}

static void	print_zero_x(t_printf *l, int *count)
{
	while (--l->w > l->len - 1)
	{
		if ((!(l->f & Z) && (l->w > l->p) && !(l->f & P)) ||
		(!(l->f & M) && (l->f & P) && (l->f & Z) &&
		(l->p - 1 < l->w)))
			ft_putchar_fd(' ', 1);
		else if (((l->f & Z) && (!(l->f & M))) || (l->w < l->p)
		|| ((l->f & M) && (l->w < l->p)))
			ft_putchar_fd('0', 1);
		else
		{
			if (!(l->f & M))
				ft_putchar_fd(' ', 1);
			else
				*count = *count + 1;
		}
		l->ret++;
	}
}

static void	print_arg_x(t_printf *l, int *count, int up, unsigned int *u)
{
	if (*count > 0)
		l->w = *count;
	if (!(l->f & M) || (l->f & P))
	{
		if (!((l->f & P) && (l->p == 0) && ((unsigned int)u == 0)))
		{
			if (up == 1)
				ft_putstr_fd(ft_utoa_base((unsigned int)u, 16, 1), 1);
			else
				ft_putstr_fd(ft_utoa_base((unsigned int)u, 16, 0), 1);
		}
		else
		{
			if (!(l->w == 0))
			{
				ft_strdup("");
				l->ret--;
			}
			else
				ft_putchar_fd(' ', 1);
		}
	}
}

static void	print_left_x(t_printf *l, int *count)
{
	while (--l->w >= 0)
	{
		if (l->w < *count)
			ft_putchar_fd(' ', 1);
	}
}

void		print_x(t_printf *l, int up)
{
	void	*u;
	int		count;

	count = 0;
	u = va_arg(l->arg, unsigned int*);
	l->len = ft_strlen(ft_utoa_base((unsigned int)u, 16, 0));
	l->ret += l->len;
	if (l->w < l->p)
		l->w = l->p;
	print_minus_x(l, up, u);
	print_zero_x(l, &count);
	print_arg_x(l, &count, up, u);
	print_left_x(l, &count);
}
