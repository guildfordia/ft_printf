/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_per.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/06 15:08:24 by alangloi          #+#    #+#             */
/*   Updated: 2020/10/07 18:35:37 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void	print_per(t_printf *l)
{
	if (l->f & M)
	{
		ft_putchar_fd('%', 1);
		l->ret++;
	}
	while (--l->w > 0)
	{
		if ((l->f & Z) && !(l->f & M))
			ft_putchar_fd('0', 1);
		else
			ft_putchar_fd(' ', 1);
		l->ret++;
	}
	if (!(l->f & M))
	{
		ft_putchar_fd('%', 1);
		l->ret++;
	}
}
