/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_flags.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/06 15:06:28 by alangloi          #+#    #+#             */
/*   Updated: 2020/10/27 17:16:01 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void	check_flags(t_printf *l)
{
	while (ft_strchr("0-.*", *l->frmt))
	{
		if (*l->frmt == '0')
			l->f |= Z;
		if (*l->frmt == '-')
			l->f |= M;
		if (*l->frmt == '*')
		{
			if (l->f & A1)
				l->f |= A2;
			else
				l->f |= A1;
		}
		if ((*l->frmt == '.') || (l->f & A2))
			l->f |= P;
		l->frmt++;
	}
}
